(function(){
	var d1 = [[1,30], [2,35], [3,50], [4,60], [5,45], [6,60], [7,70]]
	var d2 = [[1,25], [2,25], [3,48], [4,50], [5,42], [6,50], [7,50]]
	var d3 = [[1,20], [2,15], [3,40], [4,40], [5,35], [6,40], [7,30]]

	// $.plot("#chart", [
	// 	{	
	// 		data: d1,
	// 		color: '#ed6e37',
	// 		lines: { show: true, lineWidth: 5, color: '#ed6e37' },
	// 		points: { show: true, stroke: true, strokeColor: '#333', fill: true, fillColor: '#ed6e37' },
	// 		grid: { color: 'red' },
	// 		bars: { barWidth: '3px' }
	// 	},
	// 	{	
	// 		data: d2,
	// 		color: '#259e01',
	// 		lines: { show: true, lineWidth: 5 },
	// 		points: { show: true, fill: true, fillColor: '#259e01' },
	// 		grid: { color: 'transparent' }
	// 	},
	// 	{	
	// 		data: d3,
	// 		color: '#15a0c8',
	// 		lines: { show: true, lineWidth: 5 },
	// 		points: { show: true, fill: true, fillColor: '#15a0c8' },
	// 		grid: { color: 'transparent' }
	// 	}
	// ])

	var chartOne = $("#chart-one").get(0).getContext("2d");
	var chartTwo = $("#chart-two").get(0).getContext("2d");

  var dataOne = {
      labels: ["Week 48", "Week 49", "Week 50", "Week 51", "Week 52", "Week 52"],
      datasets: [
          {
        		label: 'Net Comp',
            fillColor: "transparent",
            strokeColor: "#15a0c8",
            pointColor: "#15a0c8",
            pointStrokeColor: "#fff",
            pointDotStrokeWidth : 5,
            datasetStrokeWidth : 5,
            pointDotRadius : 50,
            data: [300, 420, 350, 610, 750, 760, 660]
          },
          {
        		label: 'AnalyzerHR',
            fillColor: "transparent",
            strokeColor: "#259e01",
            pointColor: "#259e01",
            pointStrokeColor: "#fff",
            pointDotStrokeWidth : 5,
            datasetStrokeWidth : 5,
            pointDotRadius : 50,
            data: [350, 440, 460, 620, 780, 762, 650]
          },
          {
        		label: 'Question Right',
            fillColor: "transparent",
            strokeColor: "#ed6e37",
            pointColor: "#ed6e37",
            pointStrokeColor: "#fff",
            pointDotStrokeWidth : 5,
            datasetStrokeWidth : 5,
            pointDotRadius : 50,
            data: [450, 530, 480, 700, 880, 862, 710]
          }
      ]
  }

  var newChartOne = new Chart(chartOne).Line(dataOne, {
  	pointDotRadius: 5,
  	pointDotStrokeWidth: 3,
  	datasetStrokeWidth : 6,
  	bezierCurve : false,
  	legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li style=\"color:<%=datasets[i].strokeColor%>\"><span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></span></li><%}%></ul>"
  });

  var legendOne = newChartOne.generateLegend();
  $('.chart-one').append(legendOne);

  var dataTwo = {
      labels: ["Week 48", "Week 49", "Week 50", "Week 51", "Week 52", "Week 52"],
      datasets: [
          {
        		label: 'Net Comp',
            fillColor : "#ed6e37",
            strokeColor : "#ed6e37",
            data : [1,3,2,7,5,9]
          },
          {
          	label: 'AnalyzerHR',
            fillColor : "#259e01",
            strokeColor : "#259e01",
            data : [2,4,1,6,5,8]
          },
          {
          	label: 'Question Right',
            fillColor : "#15a0c8",
            strokeColor : "#15a0c8",
            data : [1,2,3,6,4,7]
          }
      ]
  }

  var newChartTwo = new Chart(chartTwo).Bar(dataTwo, {
  	barStrokeWidth : 7,
  	barValueSpacing : 23,
  	barDatasetSpacing : 7,
  	legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li style=\"color:<%=datasets[i].fillColor%>\"><span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></span></li><%}%></ul>"
  });

  var legendTwo = newChartTwo.generateLegend();
  $('.chart-two').append(legendTwo);
	
})()